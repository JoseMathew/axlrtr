\name{axlrtr-package}
\alias{axlrtr-package}
\alias{axlrtr}
\docType{package}
\title{Axelrod's Tournament}

\description{
This package provides functions to run strategies (henceforth 'rules') against each other in an Iterated Prisoner's Dilemma (henceforth 'Axelrod's Tournament'), and to rank them by performance. It also has a list of common rules, and allows users to define their own rule functions (which should be named in the form "rule_foo", and then called as "foo".)
}

\author{Jose Mathew}

\references{
Axelrod, Robert & Hamilton, Wiliam D. The Evolution of Cooperation. Science 211, 1390–1396 (1981). \doi{10.1126/science.7466396} 

Axelrod, Robert. The Evolution of Cooperation. New York: Basic Books (1984)
}

\keyword{ package }

\examples{
#The easiest way to use \pkg{axlrtr} is to use the \code{\link{at_scores}} function. It accepts a vector containing the codes of the rules to be used, plays them against each other, and returns a rank table with the score of each rule. For example, to conduct a tournament bettween rules 19, 3, 22 and 32, with all default settings, use the command:-
at_scores(c("19", "03", "22", "32"))
}
