#' Title: Original Tournament Entries
#' Version: 0.4
#' Date: 2022-03-27
#' Author: Jose Mathew
#' Maintainer: Jose Mathew <josemathew@protonmail.com>
#' Description: Some of the original entries for the the Axelrod's Tournament.
#' Licence: BSD-3-Clause

# Davis
rule_31 <- function(self_history, other_history)
{
i <- c(1:10)
trunc_oth_hist <- other_history[-i]
if (length(other_history) > 9) {
action <- all(trunc_oth_hist)
} else
action <- TRUE
return(action)
}

# Downing
rule_32 <- function(self_history, other_history)
{
if (length(other_history) == 0) {
action <- TRUE
} else {
outcomes <- c(0,0,0,0)
for (i in 1:length(self_history))
{
if (self_history[i] == TRUE) {
if (other_history[i] == TRUE) {
outcomes[1] <- outcomes[1] + 1 } else
outcomes[2] <- outcomes[2] + 1
} else
if (other_history[i] == TRUE) {
outcomes[3] <- outcomes[3] + 1 } else
outcomes[4] <- outcomes[4] + 1
}
pcc <- (outcomes[1] + 0.0005) / (outcomes[1] + outcomes[2] + 0.001)
pcd <- (outcomes[3] + 0.0005) / (outcomes[3] + outcomes[4] + 0.001)
action <- pcc >= pcd
}
return(action)
}

# Eatherley
rule_33 <- function(self_history, other_history)
{
action <- TRUE
a <- sum(other_history) / length(other_history)
if (length(other_history) != 0 && other_history[length(other_history)] == FALSE)
action <- runif(1, 0, 1) <= a
return(action)
}

# Feld
rule_34 <- function(self_history, other_history)
{
a <- length(other_history)
if (a == 0) {
action <- TRUE
} else if (other_history[a] == TRUE) {
action <- runif(1, 0, 1) <= (0.5  * (1 + 1 / a))
} else
action <- FALSE
return(action)
}

# Grofman
rule_35 <- function(self_history, other_history)
{
a <- length(other_history)
if (a < 2) {
action <- TRUE
} else if (a < 7) {
action <- other_history[a]
} else if (other_history[a] == self_history[a]) {
action <- TRUE
} else
action <- sample(7, 1) < 2.5
return(action)
}

# Nydegger
rule_36 <- function(self_history, other_history)
{
a <- length(other_history)
if (a == 0) {
action <- TRUE
} else if (a == 1) {
action <- other_history[1]
} else if (a == 2) {
if (other_history[1] == FALSE && other_history[2] == TRUE) {
action <- FALSE
} else
action <- other_history[2]
} else {
action <- TRUE
A = 32 * (1 - other_history[a]) + 16 * (1 - self_history[a])
A = A + 8 * (1 - other_history[a-1]) + 4 * (1 - self_history[a-1])
A = A + 2 * (1 - other_history[a-2]) + (1 - self_history[a-2])
B <- c(1,6,7,17,22,23,26,29,30,31,33,38,39,45,49,54,55,58,61)
if (is.element(A,B))
action <- FALSE
}
return(action)
}

# Pavlov
rule_37 <- function(self_history, other_history)
{
a <- length(other_history)
if (a < 1) {
action <- TRUE
} else if (other_history[a] == TRUE) {
action <- self_history[a]
} else
action <- !(self_history[a])
return(action)
}

# Shubik
rule_38 <- function(self_history, other_history)
{
action <- TRUE
a <- length(other_history) - sum(other_history)
action <- all(tail(other_history,a))
return(action)
}

# Tullock
rule_39 <- function(self_history, other_history)
{
a <- length(other_history)
b <- sum(tail(other_history,10))
if (a < 11) {
action <- TRUE
} else 
action = sample(1:10, 1) <= (b-1)
return(action)
}
